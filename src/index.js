const express = require('express');
const cors = require('cors');
const dotenv = require('dotenv');
const routes = require('./routes');

const app = express();
dotenv.config();
const PORT = process.env.PORT || 3000

app.use(express.json());
app.use(cors());

//use routes
app.use('/', routes);

app.listen(PORT, () => {
    console.log(`Listening on port ${PORT}`);
});