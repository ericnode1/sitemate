const issueMockData = require('../../mockData.json');
// router get api/issue
// des    get issue list
const listIssues = (req, res) => {
    console.log(issueMockData);
    return res.status(200).json(issueMockData);
};

// router get api/issue
// des    get an issue with issue ID
const getOneIssueById = (req, res) => {
    const { issueId } = req.params;

    if (!issueMockData[issueId]) return res.status(404).send({ error: "issue not found" });

    const result = issueMockData[issueId]
    console.log(result);
    return res.status(200).json({ data: result });
};

// router POST api/issue
// des    create an issue
const createOneIssue = (req, res) => {
    const data = req.body;
    if (!data.id || !data.title || !data.description) {
        return res.status(400).json({ error: 'Information is not fully provided' });
    }
    if (issueMockData[data.id]) {
        return res.status(400).json({ error: "This issue already exists!" });
    }
    // save data
    console.log(data)
    return res.status(200).send({ data });
};

// router PUT api/issue
// des    update one issue with its ID
const updateOneIssue = (req, res) => {
    const data = req.body;
    if (!data.id || !data.title || !data.description) {
        return res.status(400).json({ error: 'Information is not fully provided' });
    }
    if (!issueMockData[data.id]) {
        return res.status(400).json({ error: "This issue does not exist!" });
    }
    const issueId = req.params.id;
    //update issueMockData with issueId
    return res.status(200).json({ data });
};

// router DELETE api/issue
// des    delete one issue with its ID
const deleteOneIssue = (req, res) => {
    const issueId = req.params.id;
    if (issueId) {
        return res.status(404).json({ error: "issueId is needed" });
    }
    //delete one issue
    return res.status(200).json({ data: issueMockData});
};

module.exports = {
    listIssues,
    getOneIssueById,
    createOneIssue,
    updateOneIssue,
    deleteOneIssue,
};