const express = require('express');
const cors = require('cors');
const masterRouter = require('./routes');

const app = express();
app.use(express.json());
app.use(cors());
app.use("/api", masterRouter);

module.exports = app;