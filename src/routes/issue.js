const express = require("express");
const {
    listIssues,
    getOneIssueById,
    createOneIssue,
    updateOneIssue,
    deleteOneIssue
} = require("../controller/issueController");
const router = express.Router();

router.get("/", listIssues);
router.get("/:id", getOneIssueById);
router.post("/", createOneIssue);
router.put("/:id", updateOneIssue);
router.delete("/:id", deleteOneIssue);

module.exports = router;