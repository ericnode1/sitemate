const express = require("express");
const masterRouter = express.Router();
const issueRoute = require("./issue");

masterRouter.use("/issue", issueRoute);

masterRouter.get("/", () => {
    console.log('this is master')
});

module.exports = masterRouter;